#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void){
	
	int dim1 = 10;
	int dim2 = 300;
	char m[dim1][dim2];
	
	// Riempio il dizionario con la stringcopy (strcpy)
	strcpy(m[0], "'Per me si va ne la città dolente, per me si va ne l'etterno dolore, per me si va tra la perduta gente.");
	strcpy(m[1], "Giustizia mosse il mio alto fattore: fecemi la divina podestate, la somma sapienza e 'l primo amore.");
	strcpy(m[2], "Dinanzi a me non fuor cose create se non etterne, e io etterno duro.");
	strcpy(m[3], "Lasciate ogne speranza, voi ch’intrate'.");
	strcpy(m[4], "Queste parole di colore oscuro vid’io scritte al sommo d’una porta; per ch’io: 'Maestro, il senso lor m’è duro'.");
	strcpy(m[5], "Ed elli a me, come persona accorta: 'Qui si convien lasciare ogne sospetto; ogne viltà convien che qui sia morta.");
	strcpy(m[6], "Noi siam venuti al loco ov’i’ t’ho detto che tu vedrai le genti dolorose c’hanno perduto il ben de l’intelletto'.");
	
	/*
	*	m					 0  1  2  3  4  5  6  7  8  9  ....
	*	[0]		--> [', P, e, r,  , m, e,  , s, i,  , v, a,  , n, e,  , l, a, ...]
	*	[1]		--> ...
	* [2]		--> ...
	* .			--> ...
	* .			--> ...
	* [9]		-->
	*/
	
	//Cosí stampi tutto il dizionario
	for(int i = 0; i < dim1; i++)
		printf("%s\n", m[i]);
	
	return 0;
}