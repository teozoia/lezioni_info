#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Trova l'ultima lettera di una stringa
int main(void){
	
	char s[100] = "Ciao, questa é una frase di prova";
	char n;
	int i = strlen(s)-1;
	int pos = -1;
	
	printf("Inserisci lettera: ");
	scanf("%c", &n);
	
	//i = 35
	while(s[i] != n){
		i--;
	}
	pos = i;
	
	printf("%d\n",pos);
	
	return 0;
}
