#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//trova la prima lettera di una stringa
int main(void){
	
	char s[100] = "Ciao, questa é una frase di prova";
	char n;
	int i = strlen(s);
	int pos = -1;
	
	printf("Inserisci lettera: ");
	scanf("%c", &n);
	
	/*
	for(i = 0; i < 100; i++)
		if(s[i] == n)
			pos = i;
	*/
	
	//i = 35
	while(s[i] != n){
		i++;
	}
	pos = i;
	
	printf("%d\n",pos);
	
	return 0;
}
