#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Trova l'ultima lettera di una stringa
int main(void){
	
	char s[100] = "Ciao, questa é una frase di prova";
	int chiave = 0;
	
	printf("Inserisci chiave: ");
	scanf("%d", &chiave);
	
	// Cifratura
	for(int i = 0; i < 100; i++){
		s[i] = s[i] + chiave;
	}
	printf("%s\n",s);
	
	// Decifratura
	for (int i = 0; i < 100; i++){
		s[i] = s[i] - chiave;
	}
	printf("%s\n",s);
	
	return 0;
}
