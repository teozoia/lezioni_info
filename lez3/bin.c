#include <stdio.h>
#include <stdlib.h>
int main(void){
	
	int dim = 1000;
	int a[dim];
	int key;
	int basso = 0;
	int alto = dim - 1;
	int medio = (alto + basso) / 2;
	
	for(int i = 0; i < dim; i++)
		a[i] = i;
	
	printf("Inserisci il numero da cercare: ");
	scanf("%d", &key);
	
  while(key != a[medio]){ // se la cond é vera continuo a fare il ciclo
		
		if(key < a[medio]){
			alto = medio;
		}else{
			
			if(key > a[medio]){
				basso = medio;
			}
			
		}
		medio = (alto + basso) / 2;
	}
	
	printf("posizione: %d", medio);
	printf("elemento: %d", a[medio]);
		
	return 0;
}

